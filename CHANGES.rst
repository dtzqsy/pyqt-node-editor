Changelog (PyNodeEditor)
========================

1.0.0 (unreleased)
------------------

- First version of the library
- After 52 tutorials: https://www.blenderfreak.com/tutorials/node-editor-tutorial-series/